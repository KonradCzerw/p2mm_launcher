// SPDX-License-Identifier: GPL-3.0+

#ifdef _WIN32
#ifndef WINDOWS
// the WINDOWS macro is used for platform-dependant sections of code
// specific to Microsofts' operating system
#define WINDOWS
#endif
#endif

#ifdef WINDOWS
#include <windows.h>
#endif

// ENTRYPOINT
#ifdef WINDOWS
int WinMain(HINSTANCE hInstance, hPrevInstance, LPSTR lpCmdLine, int nShowCmd) {
#else
int main(int argc, char** argv) {
#endif

	return 0;
}

